import { wait } from "../../../typescript-lib/src/shared/common"
import { createElement } from "../../../typescript-lib/src/shared/dom"
import { BBCodeData, ImageMapData, ListData, ListRowData, TextAreaData } from "../../types/bbcode"
import { BBCodeElementWrapper } from "./BBCodeElementWrapper"
import { ImageMapEditor } from "./ImageMapEditor"

export class PostGenerator extends HTMLElement {
    constructor(mode: "userpage" | "forum" | "full", data: ImageMapData[]) {
        super()

        this.classList.add("post-generator", `post-generator--${mode}`)
        // currently the generator is limited to just a single imagemap
        // const textArea = new BBCodeElementWrapper("textarea")
        // textArea.registerChild(new TextArea(textArea))
        // const list = new BBCodeElementWrapper("list")
        // list.registerChild(new ListEditor(list))
        const exportButton = createElement("button", {
            attributes: {
                onclick: async () => {
                    navigator.clipboard.writeText(this.getBBCode())
                    const previousText = exportButton.innerText
                    exportButton.innerText = "copied to clipboard"
                    await wait(2000)
                    exportButton.innerText = previousText
                },
                innerText: "get bbcode",
            },
            className: "button button--primary",
        })
        this.append(
            new ImageMapEditor(data[0]),
            // textArea,
            // list,
            exportButton
        )
    }

    getBBCode(): string {
        const data: BBCodeData[] = []

        this.querySelectorAll<BBCodeElementWrapper>("." + this.className.split(" ").join(".") + " > .bbcode-element-wrapper").forEach((e) => data.push(e.getChild().getBBCodeData()))
        this.querySelectorAll<ImageMapEditor>("." + this.className.split(" ").join(".") + " > .image-map-editor").forEach((e) => data.push(e.getBBCodeData()))

        let bbCode = ""

        data.forEach((entry) => {
            if (entry instanceof ImageMapData) {
                bbCode += this.parseImageMapData(entry) + "\n"
            } else if (entry instanceof ListData) {
                bbCode += this.parseListData(entry) + "\n"
            } else if (entry instanceof TextAreaData) {
                bbCode += this.parseTextAreaData(entry) + "\n"
            }
        })

        bbCode += `\n[img]<-- WARNING: anything within this img tag is data used to read in your generated bbcode back into the post generator. Please do not touch this. If you wish to import this please copy everything between the opening and closing tag, including the tags themself-->${JSON.stringify(
            data
        )}[/img]`

        return bbCode
    }

    private parseImageMapData(data: ImageMapData): string {
        const validAreas = data.areas.filter((area) => {
            return !(area.url == "" && area.title == "")
        })

        // if there are 0 valid areas return
        if (validAreas.length == 0) {
            return ""
        }

        let bbCode = "[imagemap]\n"
        bbCode += data.url + "\n"

        validAreas.forEach((area) => {
            bbCode += `${area.x} ${area.y} ${Math.abs(area.width)} ${Math.abs(area.height)} ${area.url == "" ? "#" : area.url} ${area.title}\n`
        })

        bbCode += "[/imagemap]"

        return bbCode
    }

    private parseListData(data: ListData): string {
        // if there are no items return
        if (data.items.length == 0) {
            return ""
        }

        const childBBCodes: string[] = []
        data.items.forEach((entry) => {
            if (entry instanceof ListData) {
                childBBCodes.push(this.parseListData(entry))
            } else if (entry instanceof ListRowData) {
                childBBCodes.push(this.parseTextAreaData(entry))
            }
        })

        const validChildBBCodes = childBBCodes.filter((code) => code != "")
        // if there are no items with a valid return, return
        if (validChildBBCodes.length == 0) {
            return ""
        }
        let bbCode = data.type == "numbered" ? "[list=1]\n" : "[list]\n"

        validChildBBCodes.forEach((entry) => {
            bbCode += entry + "\n"
        })

        bbCode += "[/list]"

        return bbCode
    }

    private parseTextAreaData(data: TextAreaData): string {
        return data.value
    }
}

customElements.define("post-generator", PostGenerator)
