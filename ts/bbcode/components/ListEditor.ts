import { createElement, createIcon } from "../../../typescript-lib/src/shared/dom"
import { BBCodeEditor, ListData } from "../../types/bbcode"
import { BBCodeElementWrapper } from "./BBCodeElementWrapper"
import { ListElementEditor } from "./ListElementEditor"

export class ListEditor extends HTMLElement implements BBCodeEditor {
    private editor
    private buttonArea = createElement("div", {
        className: "bbcode-list-editor__button-area",
    })

    private typeSelector = createElement("select", {
        children: [
            createElement("option", {
                attributes: {
                    innerText: "Bullet",
                    value: "bullet",
                },
            }),
            createElement("option", {
                attributes: {
                    innerText: "Numbered",
                    value: "numbered",
                },
            }),
        ],
    })

    constructor(editor: BBCodeElementWrapper) {
        super()
        this.editor = editor

        this.classList.add("bbcode-list-editor")

        this.setupPopup()

        this.buttonArea.append(
            createElement("button", {
                className: "button button--secondary",
                attributes: {
                    innerText: "add list entry",
                    onclick: () => {
                        this.addRow()
                    },
                },
                children: [createIcon("documentAdd")],
            }),
            createElement("button", {
                className: "button button--secondary",
                attributes: {
                    innerText: "add list",
                    onclick: () => {
                        this.addList()
                    },
                },
                children: [createIcon("documentAdd")],
            })
        )
        this.append(this.buttonArea)
        this.addRow()
        this.addRow()
    }

    getBBCodeData(): ListData {
        return new ListData(
            this.typeSelector.value as ListData["type"],
            [
                // direct rows of this element
                ...this.querySelectorAll<ListElementEditor>("." + this.className.split(" ").join(".") + " > .bbcode-element-wrapper > .bbcode-list-editor__entry"),
                // other lists
                ...this.querySelectorAll<ListEditor>("." + this.className.split(" ").join(".") + " > .bbcode-element-wrapper > .bbcode-list-editor"),
            ].map((element) => {
                return element.getBBCodeData()
            })
        )
    }

    private addRow() {
        const wrapper = new BBCodeElementWrapper("list entry")
        wrapper.registerChild(new ListElementEditor(wrapper))
        this.buttonArea.before(wrapper)
    }

    private addList() {
        const wrapper = new BBCodeElementWrapper("sub-list")
        wrapper.registerChild(new ListEditor(wrapper))
        this.buttonArea.before(wrapper)
    }

    private setupPopup() {
        const chevronIcon = createIcon("chevronDown")
        chevronIcon.classList.add("icon")
        this.editor.addPopupElement(
            createElement("div", {
                className: "icon-input-container icon-input-container--right-align",
                children: [this.typeSelector, chevronIcon],
            })
        )
    }
}

customElements.define("list-editor", ListEditor)
