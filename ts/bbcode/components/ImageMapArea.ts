import { createElement, createIcon } from "../../../typescript-lib/src/shared/dom"
import { BBCodeEditor, ImageMapAreaData } from "../../types/bbcode"
import { ImageMapAreaForm } from "./ImageMapAreaControls"
import { ImageMapEditor } from "./ImageMapEditor"

export class ImageMapArea extends HTMLElement implements BBCodeEditor {
    private parent: ImageMapEditor
    private y: number = 0
    private x: number = 0
    private calcWidth: number = 0
    private calcHeight: number = 0
    private form: ImageMapAreaForm
    // important to not use this.x / this.y for drag as that will always return relative to event start compared to themself
    private prevX = 0
    private prevY = 0
    private xElement
    private yElement
    private wElement
    private hElement

    private initFinished = false

    /**
     * constructor
     * @param parent parent imagemap editor this area is part of
     * @param e event that spawns this area, null in case this is constructed via existing data
     * @param data optional, existing data to create this area with
     */
    constructor(parent: ImageMapEditor, e: MouseEvent | null, data?: ImageMapAreaData) {
        super()
        this.parent = parent
        this.classList.add("image-map-editor__area")
        this.form = new ImageMapAreaForm(this, data)

        this.xElement = this.form.getXField()
        this.yElement = this.form.getYField()
        this.wElement = this.form.getWidthField()
        this.hElement = this.form.getHeightField()
        if (data) {
            this.setXY(data.x, data.y)
            this.setWH(data.width, data.height)
        }
        this.xElement.addEventListener("change", () => {
            this.setXY(parseFloat(this.xElement.value), this.y)
        })
        this.yElement.addEventListener("change", () => {
            this.setXY(this.x, parseFloat(this.yElement.value))
        })
        this.wElement.addEventListener("change", () => {
            this.setWH(parseFloat(this.wElement.value), this.calcHeight)
        })
        this.hElement.addEventListener("change", () => {
            this.setWH(this.calcWidth, parseFloat(this.hElement.value))
        })
        if (e != null) {
            this.setXY(this.getCoordinateAsPercentage(e.clientX, "x"), this.getCoordinateAsPercentage(e.clientY, "y"))
        }

        let controlMenuButton = createElement("div", {
            className: "image-map-editor__area-settings-button",
            attributes: {
                innerText: "",
                onclick: () => {
                    this.form.classList.toggle("visible")
                },
                title: "settings",
            },
            children: [createIcon("setting")],
        })

        let dragableArea = createElement("div", {
            className: "image-map-editor__area-dragable-area",
        })

        this.append(
            createElement("div", {
                className: "image-map-editor__area-wrapper",
                children: [
                    controlMenuButton,
                    dragableArea,
                    createElement("div", {
                        className: "image-map-editor__area-resize-handle",
                        attributes: {
                            title: "resize",
                        },
                    }),
                ],
            })
        )

        parent.append(this.form)

        parent.addEventListener("click", (e) => {
            const target = e.target
            if (target == null) {
                return
            }

            if (!(this.form.contains(e.target as Node) || this.contains(e.target as Node))) {
                this.form.classList.remove("visible")
            }
        })

        this.tabIndex = 0 // required to catch keydown events

        this.addEventListener("keydown", (e) => {
            if(e.key == "Delete"){
                if(window.confirm("Delete this area?")){
                    this.remove()
                    this.form.remove()
                }
            }
        })

        // important to not use this.x / this.y as that will always return relative to event start compared to themself
        let mouseDown = false
        dragableArea.addEventListener("mousedown", (e) => {
            this.prevX = 0
            this.prevY = 0
            if (e.target != dragableArea) {
                mouseDown = false
                return
            }
            mouseDown = true
        })
        dragableArea.addEventListener("mousemove", (e) => {
            if (mouseDown) {
                this.move(e)
            }
        })
        dragableArea.addEventListener("mouseup", (e) => {
            mouseDown = false
        })
        dragableArea.addEventListener("mouseleave", (e) => {
            mouseDown = false
        })

        new ResizeObserver(() => {
            this.fetchSize()
            // manual resizing explicitly sets width and height. after these are consumed and turned into % they are getting nuked because else some styling doesnt work
            this.style.width = ""
            this.style.height = ""
        }).observe(this)
    }
    getBBCodeData(): ImageMapAreaData {
        return new ImageMapAreaData(this.x, this.y, this.calcWidth, this.calcHeight, this.form.getTitle().trim(), this.form.getURL().trim())
    }

    public setWidth(newWidth: number) {
        this.setXY(0.0, this.y)
        this.setWH(newWidth, this.calcHeight)
    }

    public setHeight(newHeight: number) {
        this.setXY(this.x, 0.0)
        this.setWH(this.calcWidth, newHeight)
    }

    private move(e: MouseEvent) {
        let thisX = this.getCoordinateAsPercentage(e.clientX, "x")
        let thisY = this.getCoordinateAsPercentage(e.clientY, "y")

        if (this.prevY == 0 || this.prevX == 0) {
            this.prevX = thisX
            this.prevY = thisY
        } else {
            let horizontalDistance = this.prevX - thisX
            let verticalDistance = this.prevY - thisY

            this.setXY(this.x - horizontalDistance, this.y - verticalDistance)

            this.prevX = thisX
            this.prevY = thisY
        }
        if (this.prevX > 100.0) {
            this.prevX = 100.0
        }
        if (this.prevY > 100.0) {
            this.prevY = 100.0
        }
    }

    /**
     * Extends the rectange to the given event point from the og anchor
     * @param e
     */
    public expand(e: MouseEvent) {
        let thisX = this.getCoordinateAsPercentage(e.clientX, "x")
        let thisY = this.getCoordinateAsPercentage(e.clientY, "y")
        if (thisX < this.x) {
            thisX = this.x
        }
        if (thisY < this.y) {
            thisY = this.y
        }
        let width = thisX - this.x
        let height = thisY - this.y

        this.setWH(width, height)
    }

    private fetchSize() {
        if (this.initFinished) {
            const parentRect = this.parent.getBoundingClientRect()
            const thisRect = this.getBoundingClientRect()

            this.setWH((thisRect.width / parentRect.width) * 100, (thisRect.height / parentRect.height) * 100)
        }
    }

    /**
     * Finish initialisation. This function must be called after the post generator has been added to the document, or else the ResizeObserver will fire unwanted changes
     */
    public finishInit() {
        this.initFinished = true
    }

    // percentage number!
    private setXY(x: number, y: number) {
        x = this.round(x)
        y = this.round(y)
        if (x < 0) {
            x = 0
        }
        if (y < 0) {
            y = 0
        }
        if (x + Math.abs(this.calcWidth) > 100.0) {
            x = this.round(100.0 - Math.abs(this.calcWidth))
        }
        if (y + Math.abs(this.calcHeight) > 100.0) {
            y = this.round(100.0 - Math.abs(this.calcHeight))
        }
        this.y = y
        this.x = x
        this.style.setProperty("--area-x", this.x.toString() + "%")
        this.style.setProperty("--area-y", this.y.toString() + "%")
        this.form.style.setProperty("--area-x", this.x.toString() + "%")
        this.form.style.setProperty("--area-y", this.y.toString() + "%")
        this.xElement.value = this.x.toString()
        this.yElement.value = this.y.toString()
    }

    // percentage number!
    private setWH(width: number, height: number) {
        width = this.round(width)
        height = this.round(height)
        if (width + this.x > 100.0) {
            width = this.round(100.0 - this.x)
        }
        if (height + this.y > 100.0) {
            height = this.round(100.0 - this.y)
        }
        // negative is accepted as that is quandrant change
        // TODO: check if comment above is still valid

        this.style.setProperty("--area-width", width.toString() + "%")
        this.style.setProperty("--area-height", height.toString() + "%")
        this.form.style.setProperty("--area-width", width.toString() + "%")
        this.form.style.setProperty("--area-height", height.toString() + "%")
        this.calcWidth = width
        this.calcHeight = height

        this.wElement.value = this.calcWidth.toString()
        this.hElement.value = this.calcHeight.toString()
    }

    public getCoordinateAsPercentage(coord: number, type: "x" | "y") {
        const boundingRect = this.parent.getBoundingClientRect()
        let offset = type == "x" ? boundingRect.left : boundingRect.top
        let size = type == "x" ? boundingRect.width : boundingRect.height
        return this.round(((coord - offset) / size) * 100)
    }

    public round(value: number) {
        return parseFloat(value.toFixed(1))
    }
}
customElements.define("imagemap-editor-area", ImageMapArea)
